// This file was auto-generated by ML.NET Model Builder. 

using System;
using SolverCoreML.Model;
using System.IO;
using System.Text;
using Microsoft.ML;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;

namespace SolverCoreML.ConsoleApp
{
    class CreateModel
    {
        //pathToRootImageDirectory must be a path to a folder (Root in the example) which has the following structure
        //Root:
        //  Label1:  
        //      image1.jpg
        //      image2.jpg
        //          .
        //          .
        //          .
        //  Label2:
        //      imagen.jpg
        //    .
        //    .
        //    .
        private static string CreateLabelFile(string pathToRootImageDirectory)
        {
            var subfolders = Directory.GetDirectories(pathToRootImageDirectory);

            var tsv = "traindata.tsv";

            string delimiter = "\t";


            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"Label{delimiter}ImageSource");
            sb.Append("\n");
            foreach (var subfolder in subfolders)
                foreach (var file in Directory.GetFiles(subfolder))
                {
                    sb.Append($"{new DirectoryInfo(subfolder).Name}{delimiter}{file}");
                    sb.Append("\n");
                }

            File.WriteAllText(tsv, sb.ToString());

            return tsv;

        }

        //pathToTsvFile should be a path to the file generated by CreateLabelFile function
        private static void TestData(string pathToTsvFile)
        {
            MLContext ctx = new MLContext(1);

            var model = ctx.Model.Load(Path.GetFullPath("MLModel.zip"), out var schema);

            IDataView trainingDataView = ctx.Data.LoadFromTextFile<ModelInput>(
                                            path: pathToTsvFile,
                                            hasHeader: true,
                                            separatorChar: '\t',
                                            allowQuoting: true,
                                            allowSparse: false);
            var transformedData = model.Transform(trainingDataView);
            var predictions = ctx.Data.CreateEnumerable<ModelOutput>(transformedData, reuseRowObject: false).ToList();

        }

        private static void BuildNewModel(string pathToRootDirectory)
        {
            var trainDataFilepath = CreateLabelFile(pathToRootDirectory);

            ModelBuilder.TRAIN_DATA_FILEPATH = trainDataFilepath;
        }
        static void Main(string[] args)
        {

            
            //// Create single instance of sample data from first line of dataset for model input
            ModelInput sampleData = new ModelInput()
            {
                ImageSource = @"D:\Faks\Umjetna inteligencija\EquationSolver\KaggleDataset\train\two\62428.jpg",
            };
            
      
            ModelBuilder.CreateModel();
            
                       

            // Make a single prediction on the sample data and print results
            var predictionResult = ConsumeModel.Predict(sampleData);

            Console.WriteLine("Using model to make single prediction -- Comparing actual Label with predicted Label from sample data...\n\n");
            Console.WriteLine($"ImageSource: {sampleData.ImageSource}");
            Console.WriteLine($"\n\nPredicted Label value {predictionResult.Prediction} \nPredicted Label scores: [{String.Join(",", predictionResult.Score)}]\n\n");
            Console.WriteLine("=============== End of process, hit any key to finish ===============");
            Console.ReadKey();
        }
    }
}
