﻿namespace SolverCoreML.MainForm
{

    partial class EquationSolverForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComputeButton = new System.Windows.Forms.Button();
            this.InputRecognitionTextBox = new System.Windows.Forms.TextBox();
            this.InputRecognitionLabel = new System.Windows.Forms.Label();
            this.SolutionLabel = new System.Windows.Forms.Label();
            this.SolutionTextBox = new System.Windows.Forms.TextBox();
            this.FirstOperandControl = new SolverCoreML.MainForm.CanvasControl();
            this.OperatorControl = new SolverCoreML.MainForm.CanvasControl();
            this.SecondOperandControl = new SolverCoreML.MainForm.CanvasControl();
            this.ResetButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComputeButton
            // 
            this.ComputeButton.Location = new System.Drawing.Point(293, 248);
            this.ComputeButton.Name = "ComputeButton";
            this.ComputeButton.Size = new System.Drawing.Size(75, 23);
            this.ComputeButton.TabIndex = 6;
            this.ComputeButton.Text = "Solve";
            this.ComputeButton.UseVisualStyleBackColor = true;
            this.ComputeButton.Click += new System.EventHandler(this.ComputeButton_Click);
            // 
            // InputRecognitionTextBox
            // 
            this.InputRecognitionTextBox.Location = new System.Drawing.Point(283, 319);
            this.InputRecognitionTextBox.Name = "InputRecognitionTextBox";
            this.InputRecognitionTextBox.Size = new System.Drawing.Size(100, 23);
            this.InputRecognitionTextBox.TabIndex = 7;
            // 
            // InputRecognitionLabel
            // 
            this.InputRecognitionLabel.AutoSize = true;
            this.InputRecognitionLabel.Location = new System.Drawing.Point(284, 301);
            this.InputRecognitionLabel.Name = "InputRecognitionLabel";
            this.InputRecognitionLabel.Size = new System.Drawing.Size(99, 15);
            this.InputRecognitionLabel.TabIndex = 8;
            this.InputRecognitionLabel.Text = "Recognized input";
            // 
            // SolutionLabel
            // 
            this.SolutionLabel.AutoSize = true;
            this.SolutionLabel.Location = new System.Drawing.Point(305, 373);
            this.SolutionLabel.Name = "SolutionLabel";
            this.SolutionLabel.Size = new System.Drawing.Size(51, 15);
            this.SolutionLabel.TabIndex = 10;
            this.SolutionLabel.Text = "Solution";
            // 
            // SolutionTextBox
            // 
            this.SolutionTextBox.Location = new System.Drawing.Point(283, 391);
            this.SolutionTextBox.Name = "SolutionTextBox";
            this.SolutionTextBox.Size = new System.Drawing.Size(100, 23);
            this.SolutionTextBox.TabIndex = 9;
            // 
            // FirstOperandControl
            // 
            this.FirstOperandControl.Location = new System.Drawing.Point(-14, 21);
            this.FirstOperandControl.Name = "FirstOperandControl";
            this.FirstOperandControl.Size = new System.Drawing.Size(234, 203);
            this.FirstOperandControl.TabIndex = 11;
            // 
            // OperatorControl
            // 
            this.OperatorControl.Location = new System.Drawing.Point(198, 21);
            this.OperatorControl.Name = "OperatorControl";
            this.OperatorControl.Size = new System.Drawing.Size(234, 203);
            this.OperatorControl.TabIndex = 12;
            // 
            // SecondOperandControl
            // 
            this.SecondOperandControl.Location = new System.Drawing.Point(405, 21);
            this.SecondOperandControl.Name = "SecondOperandControl";
            this.SecondOperandControl.Size = new System.Drawing.Size(234, 203);
            this.SecondOperandControl.TabIndex = 13;
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(564, 404);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 14;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // EquationSolverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 439);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.SecondOperandControl);
            this.Controls.Add(this.OperatorControl);
            this.Controls.Add(this.FirstOperandControl);
            this.Controls.Add(this.SolutionLabel);
            this.Controls.Add(this.SolutionTextBox);
            this.Controls.Add(this.InputRecognitionLabel);
            this.Controls.Add(this.InputRecognitionTextBox);
            this.Controls.Add(this.ComputeButton);
            this.Name = "EquationSolverForm";
            this.Text = "Equation Solver";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ComputeButton;
        private System.Windows.Forms.TextBox InputRecognitionTextBox;
        private System.Windows.Forms.Label InputRecognitionLabel;
        private System.Windows.Forms.Label SolutionLabel;
        private System.Windows.Forms.TextBox SolutionTextBox;
        private CanvasControl FirstOperandControl;
        private CanvasControl OperatorControl;
        private CanvasControl SecondOperandControl;
        private System.Windows.Forms.Button ResetButton;
    }
}

