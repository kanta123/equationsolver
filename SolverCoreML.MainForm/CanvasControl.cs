﻿namespace SolverCoreML.MainForm
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class CanvasControl : UserControl
    {
        private bool isDrawing;
        private Point previousLocation;
        public CanvasControl()
        {
            InitializeComponent();
            InitializeBlankCanvas();
        }

        public void Reset()
        {
            InitializeBlankCanvas();
        }

        private void InitializeBlankCanvas()
        {
            var image = new Bitmap(Canvas.Width, Canvas.Height);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                var ImageSize = new Rectangle(0, 0, image.Width, image.Height);
                graphics.FillRectangle(Brushes.White, ImageSize);
            }
            this.Canvas.Image = image;
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            this.isDrawing = true;
            this.previousLocation = e.Location;
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            this.isDrawing = false;
        }

        private void Canvas_MouseLeave(object sender, EventArgs e)
        {
            this.isDrawing = false;
        }

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(Canvas.Image, 0, 0);
        }


        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.isDrawing)
            {
                return;
            }

            Pen pen = new Pen(Color.Black, 5);

            using var graphics = Graphics.FromImage(Canvas.Image);
            graphics.DrawLine(pen, this.previousLocation, e.Location);
            this.previousLocation = e.Location;

            Canvas.Invalidate();
        }

      
    }
}
