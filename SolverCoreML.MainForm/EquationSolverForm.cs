﻿namespace SolverCoreML.MainForm
{
    using SolverCoreML.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class EquationSolverForm : Form
    {
        public EquationSolverForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.FirstOperandControl.CanvasLabel.Text = "First single digit operand";
            this.OperatorControl.CanvasLabel.Text = "Operator (\u00F7, x, +, -)";
            this.SecondOperandControl.CanvasLabel.Text = "Second single digit operand";
        }

        private void ComputeButton_Click(object sender, EventArgs e)
        {
            string firstString = ResolveLabel(FirstOperandControl.Canvas.Image, "firstOperand");
            string secondString = ResolveLabel(SecondOperandControl.Canvas.Image, "secondOperand");
            string op = ResolveLabel(OperatorControl.Canvas.Image, "operator");

            int first = NumberLabelToValue(firstString);
            int second = NumberLabelToValue(secondString);
            string operatorPretty = OperatorLabelToCharacter(op);

            this.InputRecognitionTextBox.Text = $"{first} {operatorPretty} {second}";
            this.SolutionTextBox.Text = Compute(first, second, op).ToString();

            
        }

        private string ResolveLabel(Image image, string fileName)
        {
            var imageToSave = new Bitmap(image.Width, image.Height);
            var graphics = Graphics.FromImage(imageToSave);
            
            graphics.Clear(Color.White);
            graphics.DrawImage(image, 0, 0, image.Width, image.Height);

            imageToSave.Save(
               fileName+ ".jpg",
               System.Drawing.Imaging.ImageFormat.Jpeg);
            ModelInput input = new ModelInput{ ImageSource = fileName + ".jpg" };

            var predictionResult = ConsumeModel.Predict(input);

            return predictionResult.Prediction;
        }

        private int NumberLabelToValue(string label)
        {
            switch(label)
            {
                case "one":
                    return 1;
                case "two":
                    return 2;
                case "three":
                    return 3;
                case "four":
                    return 4;
                case "five":
                    return 5;
                case "six":
                    return 6;
                case "seven":
                    return 7;
                case "eight":
                    return 8;
                case "nine":
                    return 9;
                case "zero":
                    return 0;
            }
            return 0;
        }

        private string OperatorLabelToCharacter(string op)
        {
            switch(op)
            {
                case "minus":
                    return "-";
                case "times":
                    return "*";
                case "plus cleaned":
                    return "+";
                case "div":
                    return "\u00F7";
            }
            return "";
        }

        private double Compute(int first, int second, string op)
        {
            switch(op)
            {
                case "minus":
                    return first - second;
                case "times":
                    return first * second;
                case "plus cleaned":
                    return first + second;
                case "div":
                    return Convert.ToDouble(first) / Convert.ToDouble(second);
            }
            return 0;
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            this.FirstOperandControl.Reset();
            this.SecondOperandControl.Reset();
            this.OperatorControl.Reset();
            this.InputRecognitionTextBox.ResetText();
            this.SolutionTextBox.ResetText();
        }
    }
}
